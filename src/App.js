import React from 'react';
import './App.scss';
import { connect } from 'react-redux'
import { addTodo } from './store/actions'
import TodoItem from './TodoItem'

class App extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      text: ''
    }
  }

  setText = (value) => {
    this.setState(() => ({
      text: value
    }))
  }

  addTodo = () => {
    if (this.state.text) {
      this.props.addTodo({
        desc: this.state.text,
        checked: false,
        editing: false
      })
      this.setText('')
    }
  }

  render () {
    return (
      <div className="App">
        <form onSubmit={(e) => {
          e.preventDefault()
          this.addTodo()
        }} className="todo-create">
          <input value={this.state.text}
            onChange={(e) => this.setText(e.target.value)}
            className="todo-create__input" type="text"/>
          <button className="todo-create__submit">Add</button>
        </form>
        <div className="todo-list">
          {this.props.todos.map((item, i) => {
            return <TodoItem data={{item, index: i}} key={i}/>
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  todos: state.todos
})

export default connect(mapStateToProps, { addTodo })(App);
