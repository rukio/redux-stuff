import todos from './todos'
import {combineReducers} from 'redux'

const reducers = combineReducers({
  todos
})

export default reducers