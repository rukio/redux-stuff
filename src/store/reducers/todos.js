import {
  ADD_TODO,
  TOGGLE_CHECK_TODO,
  DELETE_TODO,
  TOGGLE_EDITING_TODO,
  EDIT_TODO
} from '../actions/types'

const todos = (state = [], action) => {
  switch (action.type) {
    case ADD_TODO:
      return [...state, action.payload]
    case TOGGLE_CHECK_TODO:
      return state.map((item, i) => {
        if (i === action.payload) {
          item.checked = !item.checked
        }
        return item
      })
    case TOGGLE_EDITING_TODO:
      return state.map((item, i) => {
        if (i === action.payload) {
          item.editing = !item.editing
        } else {
          item.editing = false
        }
        return item
      })
    case EDIT_TODO:
      return state.map((item, i) => {
        if (i === action.payload.index) {
          item.desc = action.payload.data
        }
        return item
      })
    case DELETE_TODO:
      return state.filter((item, i) => i !== action.payload)
    default:
      return state
  }
}
export default todos