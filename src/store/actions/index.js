import {
  ADD_TODO,
  TOGGLE_CHECK_TODO,
  DELETE_TODO,
  TOGGLE_EDITING_TODO,
  EDIT_TODO
} from './types'

export const addTodo = (data) => {
  return {
    type: ADD_TODO,
    payload: data
  }
}

export const toggleCheckTodo = (index) => {
  return {
    type: TOGGLE_CHECK_TODO,
    payload: index
  }
}

export const toggleEditingTodo = (index) => {
  return {
    type: TOGGLE_EDITING_TODO,
    payload: index
  }
}

export const editTodo = (index, data) => {
  return {
    type: EDIT_TODO,
    payload: {
      index,
      data
    }
  }
}

export const deleteTodo = (index) => {
  return {
    type: DELETE_TODO,
    payload: index
  }
}