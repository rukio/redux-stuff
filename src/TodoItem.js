import React from 'react'
import './TodoItem.scss'
import { connect } from 'react-redux'
import {
  toggleCheckTodo,
  toggleEditingTodo,
  editTodo,
  deleteTodo
} from './store/actions'

class TodoItem extends React.Component {

  constructor (props) {
    super(props)

    this.state = {
      desc: ''
    }
  }

  setDesc = (value) => {
    this.setState(() => ({
      desc: value
    }))
  }

  toggleEditingTodo = (value) => {
    if (!this.props.data.item.editing) {
      this.setDesc(this.props.data.item.desc)
    } else {
      this.setDesc('')
    }
    this.props.toggleEditingTodo(value)
  }

  editTodo = (index, desc) => {
    this.props.toggleEditingTodo(index)
    this.props.editTodo(index, desc)
  }

  render () {
    let data = this.props.data

    return (
      <div className={`todo-list__item ${data.item.checked ? 'todo-list__item_checked' : ''}`}>
        {data.item.editing ? (
          <input type="text"
            value={this.state.desc}
            onChange={(e) => {this.setDesc(e.target.value)}}
            className="item-input"/>
        ) : (
        <p className="item-description">{data.index + 1}. {data.item.desc}</p>
        )}
        <button onClick={() => {this.toggleEditingTodo(data.index)}}
          className="item-button">
          {data.item.editing ? 'Cancel' : 'Edit'}
        </button>
        {data.item.editing ? (
          <button onClick={() => {this.editTodo(data.index, this.state.desc)}}
            className="item-button item-button_confirm"
          >Confirm</button>
        ) : ('')}
        <button onClick={() => {this.props.toggleCheckTodo(data.index)}} className="item-button">
          {data.item.checked ? 'Not done' : 'Done'}
        </button>
        <button onClick={() => {this.props.deleteTodo(data.index)}} className="item-button item-button_delete">Delete</button>
      </div>
    )
  }
}

export default connect(null, {
  toggleCheckTodo,
  toggleEditingTodo,
  editTodo,
  deleteTodo
})(TodoItem)